var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
canvas.style.backgroundColor = '#BDBDBD';

function fillRect(x, y, w, h) {
    context.fillRect(x, y, w, h);
}

function strokeRect(x, y, w, h) {
    context.strokeRect(x, y, w, h);
}

function fillText(text, x, y) {
    context.fillText(text, x, y);
}

function clear() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function circle(x, y) {
    context.beginPath();
    context.arc(x, y, 25, 0, 2*Math.PI, false);
    context.fill();
}