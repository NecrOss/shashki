function drawField() {
    for (var i = 0; i < 8; i++)
        for (var j =  0; j < 8; j++) {
            if (i % 2 != 0) {
                if (j % 2 == 0) {
                    context.fillStyle = '#5A3706';
                } else {
                    context.fillStyle = '#EE9E2E';
                }
            } else {
                if (j % 2 != 0) {
                    context.fillStyle = '#5A3706';
                } else {
                    context.fillStyle = '#EE9E2E';
                }
            }

            context.fillRect(10 + j*50, 10 + i*50, 50, 50);
        }
}

var figuresWhite = [];
var figuresBlack = [];

function getFigures() {
    for (var i = 0; i < 3; i++) {
        for (var j =  0; j < 8; j++) {
            if (i % 2 != 0) {
                if (j % 2 == 0) {
                    figuresWhite.push(new Figure(35 + j*50, 35 + i*50, 'white'));
                }
            } else {
                if (j % 2 != 0) {
                    figuresWhite.push(new Figure(35 + j*50, 35 + i*50, 'white'));
                }
            }
        }
    }

    for (var i = 5; i < 8; i++) {
        for (var j =  0; j < 8; j++) {
            if (i % 2 != 0) {
                if (j % 2 == 0) {
                    figuresBlack.push(new Figure(35 + j*50, 35 + i*50, 'black'));
                }
            } else {
                if (j % 2 != 0) {
                    figuresBlack.push(new Figure(35 + j*50, 35 + i*50, 'black'));
                }
            }
        }
    }
}

function drawFigures() {
    for (var i = 0; i < figuresWhite.length; i++) {
        figuresWhite[i].draw();
    }

    for (var i = 0; i < figuresBlack.length; i++) {
       figuresBlack[i].draw();
    }
}

function Figure(x, y, color) {
    this._x = x || 0;
    this._y = y || 0;
    this._color = color;
    this._queen = false;
}

Figure.prototype = {
    draw: function () {
        var x = this._x, y = this._y;
        context.fillStyle = this._color;
        circle(x, y);
    },

    setQueen: function () {
        this._queen = true;
    },

    setColor: function (color) {
        this._color = color;
    }
}
