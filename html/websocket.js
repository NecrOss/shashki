function startSocket() {
    var str = 'send';

    ws = new WebSocket("ws://localhost:8080/" + str);

    ws.onopen = function (event) {
        console.log('open connection with server');
    };

    ws.onmessage = function (event) {

        //console.log(event.data);

        try {
            var obj = JSON.parse(event.data);

            if (obj.type == 'waiting') {
                gameStage = 'waiting';
                if (obj.body == 'player 1') {
                    sh = sh1;
                } else {
                    sh = sh2;
                }
            } else if (obj.type == 'start') {
                gameStage = 'start';
            } else if (obj.type == 'move') {
                sh == sh1 ? sh2.setX(obj.x) : sh1.setX(obj.x);
                sh == sh1 ? sh2.setY(obj.y) : sh1.setY(obj.y);
            }
        } catch (e) {
            console.log(event.data + ' err: ' + e);
        }
    };

    ws.onclose = function (event) {
        console.log('close connection with server');
    };
}
