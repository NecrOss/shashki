package ru.alwyd.game.servlet;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import ru.alwyd.game.socket.GameWebSocket;

import javax.servlet.annotation.WebServlet;

/**
 * Created by AKlochkov on 19.10.2016.
 */
@WebServlet(name = "GameWebSocketServlet", urlPatterns = {"/"})
public class GameWebSocketServlet extends WebSocketServlet {
    private final static int LOGOUT_TIME = 10 * 60 * 1000;

    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.getPolicy().setIdleTimeout(LOGOUT_TIME);
        factory.setCreator((req, resp) -> new GameWebSocket());

        factory.register(GameWebSocket.class);
    }
}
