package ru.alwyd.game.servlet;

import ru.alwyd.game.Player;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by AKlochkov on 19.10.2016.
 */
public class UserProfileServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
//        System.out.println(userName);
//        resp.getWriter().write(userName);
        Player player = new Player(userName);
    }
}
